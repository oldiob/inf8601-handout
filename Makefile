# SPDX-License-Identifier: GPL-3.0-or-later
#
# Copyright (c) 2022 Olivier Dion <olivier.dion@polymtl.ca>

QUIET_GUILD   = @echo    '     GUILD    ' $@;
QUIET_TESTS   = @echo    '     TESTS    ';

prefix?=$(DESTDIR)/usr/local
libdir?=$(prefix)/lib
includedir?=$(prefix)/include
datarootdir?=$(prefix)/share

guilegodir?=$(libdir)/guile/3.0/site-ccache/libpatch
guilescmdir?=$(datarootdir)/guile/site/3.0/libpatch

# Scheme
GSOURCES=$(wildcard handout/*.scm)
GOBJECTS=$(GSOURCES:%.scm=%.go)

# tests
SIMPLE_GTESTS=$(wildcard tests/test-*.scm)

all: $(GOBJECTS)

check: all
	$(QUIET_TESTS) ./pre-inst-env ./tools/run-tests $(SIMPLE_GTESTS)

check-nocolor: all
	$(QUIET_TESTS) NOCOLOR=1 ./pre-inst-env ./tools/run-tests $(SIMPLE_GTESTS)

print-%:
	@echo $* = $($*)

%.go: %.scm
	$(QUIET_GUILD) ./pre-inst-env ./tools/compile-scheme $< $@ | awk '$$0 !~ /wrote.+/'

clean:
	find . -iname "*.log" -delete
	find . -iname "*.trs" -delete
	find . -iname "*.go" -delete

install:
	@install -d $(guilegodir)
	@install --mode 444 -t $(guilegodir) $(GOBJECTS)
	@install -d $(guilescmdir)
	@install --mode 444 -t $(guilescmdir) $(GSOURCES)

.PHONY: all check check-nocolor clean install
