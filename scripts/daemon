#!/usr/bin/env -S guile --no-auto-compile -e main -s
-*-Scheme-*-
!#

(use-modules
 (guix packages)
 (gnu packages base)
 (gnu packages commencement)
 (gnu packages compression)
 (gnu packages image)
 (gnu packages imagemagick)
 (gnu packages tbb)
 (gnu packages wget)
 (guix git-download)
 (guix build-system cmake)
 (guix gexp)
 ((guix licenses) #:prefix licenses:)
 (handout build)
 (handout db)
 (handout schedule)
 (handout server)
 (ice-9 match)
 ((system repl server) #:prefix repl:))

(define (number->variant n)
  (if (> n 10)
      (number->variant (modulo n 10))
      (format #f "variant-~a" (1+ n))))

(define lab-1
  (let ((commit "1f4a55ff9979325ef270b56eab352395bb4037ef")
        (hash "13a04k253hqhwljs1mvhbxq1vyg5sbam2w2bpv60y9qcgfz19sv6")
        (revision "6"))
    (package
      (name "labo-1")
      (version (git-version "1.0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://gitlab.com/oldiob/inf8601-labo1.git")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256 (base32 hash))))
      (build-system cmake-build-system)
      (arguments
       `(#:test-target "check"))
      (native-inputs
       (list
        coreutils
        diffutils
        findutils
        imagemagick
        libpng
        tbb-2020
        wget
        tar))
      (synopsis "")
      (description "")
      (home-page "")
      (license (list licenses:gpl3)))))

(define-generators
  ("labo-1" (lambda (variant)
              (values lab-1 (number->variant variant)))))

(define-patches
  ("labo-1" "source/pipeline-pthread.c" "source/pipeline-tbb.cpp"))

(define-schedule-home
  (string-append (getenv "HOME") "/inf8601"))

(define-schedule-scripts
  ("labo-1" "labo-1.sh"))

(define (kill-server)
  (kill (getpid) SIGTERM))

(define (start-server db-path port)
  (sigaction SIGINT (lambda _ (exit #t)))
  (with-db db-path
    (spawn-server port)
    (repl:run-server)))

(define (usage)
  (format (current-error-port)
          "Usage: ~a DB [PORT=8083]\n"
          (car (program-arguments))))

(define (main args)
  (format #t "I am ~a\n" (getpid))
  (match (cdr args)
    ((db-path port) (start-server db-path port))
    ((db-path) (start-server db-path 8083))
    (_ (usage))))
