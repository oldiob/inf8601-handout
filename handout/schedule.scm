(define-module (handout schedule)
  #:use-module (guix build utils)
  #:use-module (guix derivations)
  #:use-module (handout exceptions)
  #:use-module (handout db)
  #:use-module (handout team)
  #:use-module (handout work)
  #:use-module (ice-9 exceptions)
  #:use-module (ice-9 match)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 textual-ports)
  #:use-module (logging logger)
  #:use-module (srfi srfi-111)
  #:use-module (sqlite3)
  #:export (cancel-computation
            computation-status
            schedule-computation)
  #:export-syntax (define-schedule-home
                   define-schedule-scripts))

(define *schedule-home* (box '()))
(define *schedule-scripts* (box '()))

(define-syntax-rule (define-schedule-home dir)
  (begin
    (mkdir-p dir)
    (set-box! *schedule-home* dir)))

(define-syntax-rule (define-schedule-scripts (name script) ...)
  (set-box! *schedule-scripts*
            (list (cons name script) ...)))

(define (build-file file)
  (string-append (unbox *schedule-home*) "/builds/" file))

(define (script-file file)
  (string-append (unbox *schedule-home*) "/scripts/" file))

(define (result-file file)
  (string-append (unbox *schedule-home*) "/results/" file))

(define (batch script build)
  (let* ((port (open-input-pipe
                (format #f "sbatch --parsable ~a ~a" script build)))
         (line (get-line port)))
    (close-pipe port)
    (match (string-split line #\;)
      ((id) (string->number id))
      ((id node) (string->number id))
      (x
       (log-msg 'ERROR "failed to batch " build " :" x) #f))))

(define (cancel job-id)
  (invoke/quiet "scancel" job-id))

(define (queue job-id output)
  (parameterize ((current-output-port output)
                 (current-error-port output))
    (invoke "squeue" "--job" job-id
            "--format=%5A %t %20V %20S %20e %20L")))

(define (schedule-computation% job-id team-token tarball)
  (query* "
INSERT INTO schedule (slurm_id, team_token, tarball)
VALUES(:job-id, :token, :tarball)
"
   (job-id job-id)
   (token team-token)
   (tarbarl tarball)))

(define (computation-id team-token)
  (match (query* "
SELECT job_id
FROM schedule
WHERE team_token = :token
" (token team-token))
    ((#(x)) x)
    (_ #f)))

(define (drop-computation team-token)
  (match (query* "
DELETE
FROM schedule
WHERE team_token = :token
RETURNING job_id, tarball
" (token team-token))
    ((#(x y)) (cons x y))
    (_ #f)))

(define (schedule-computation team from-store)

  (define work-name (lookup-work-name/id (team-wid team)))

  (define target (build-file (basename from-store)))

  (define script (assoc-ref (unbox *schedule-scripts*) work-name))

  (copy-file from-store target)
  (chmod target #o600)

  (let ((id (batch script target)))
    (when id
      (catch 'sqlite-error
        (lambda ()
          (schedule-computation% id (team-token team) target))
        (lambda (key . args)
          (cancel id)
          (delete-file target)
          (match args
            ((who code msg)
             (cond
              ((= code SQLITE_CONSTRAINT_UNIQUE)
               (raise-exception
                (make-exception
                 (make-handout-client-exception
                  (format #f "Already a job scheduled.  Please cancel it first.")))))))
            (_ (throw key args))))))))

(define (&no-job!)
  (raise-exception
       (make-exception
        (make-handout-client-exception
         (format #f "No queued job.  Please submit a work first.")))))

(define (cancel-computation team)

  (define id+tarball (drop-computation (team-token team)))

  (if id+tarball
      (begin
        (cancel (car id+tarball))
        (delete-file (cdr id+tarball)))
      (&no-job!)))

(define (computation-status output team)

  (define id (computation-id (team-token team)))

  (if id
      (queue id output)
      (&no-job!)))
