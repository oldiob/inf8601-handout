(define-module (handout build)
  #:use-module (guix build utils)
  #:use-module (guix derivations)
  #:use-module (guix git-download)
  #:use-module (guix grafts)
  #:use-module (guix monads)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix profiles)
  #:use-module (guix scripts)
  #:use-module (guix scripts pack)
  #:use-module (guix store)
  #:use-module (guix transformations)
  #:use-module (guix utils)
  #:use-module (handout archive)
  #:use-module (handout team)
  #:use-module (handout work)
  #:use-module (ice-9 match)
  #:use-module (ice-9 receive)
  #:use-module (ice-9 textual-ports)
  #:use-module (logging logger)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-34)
  #:use-module (srfi srfi-111)
  #:export (build-team-work
            get-team-work)
  #:export-syntax (define-generators
                   define-patches))

(define *package-generators* (box '()))
(define *package-patches* (box '()))

(define-syntax-rule (define-generators (name proc) ...)
  (set-box! *package-generators* (list (cons name proc) ...)))

(define-syntax-rule (define-patches (name patches ...) ...)
  (set-box! *package-patches* (list (list name patches ...) ...)))

(define (make-branch-transformer package branch)
  (options->transformation
   `((with-branch . ,(string-append (package-name package) "=" branch)))))

(define (package->package-variant package branch)
  ((make-branch-transformer package branch) package))

(define (with-replaced-files pkg files)
  (package/inherit pkg
    (source
     (computed-file (string-append (package-name pkg) "-source")
                    (with-imported-modules '((guix build utils))
                      #~(begin
                          (use-modules (guix build utils))
                          (copy-recursively #+(package-source pkg) #$output)
                          (chdir #$output)
                          (for-each (lambda (origin target)
                                      (delete-file target)
                                      (copy-file origin target))
                                    '(#+@(map car files))
                                    '(#+@(map cdr files)))))))))

(define (package->package-patched package replace-files)
  (with-replaced-files
   package
   (map (lambda (file+tgt)
          (cons (local-file (car file+tgt))
                (cdr file+tgt)))
        replace-files)))

(define wrapped-manifest-entry
  (@@ (guix scripts pack) wrapped-manifest-entry))

(define* (build-team-work% package #:rest options)
  (mbegin %store-monad
    (apply set-build-options* options)
    (mlet* %store-monad ((pkg -> (wrapped-manifest-entry (package->manifest-entry package)))
                         (manifest -> (make-manifest (list pkg)))
                         (profile -> (profile (content manifest)
                                              (relative-symlinks? #t)))
                         (drv (self-contained-tarball (package-name package)
                                                      profile
                                                      #:symlinks '(("/bin" -> "bin"))))
                         (_ (built-derivations (list drv))))
      (return (derivation->output-path drv)))))

(define* (build-team-work archive team log #:rest options)
  "Build work in ARCHIVE from TEAM.  Accept any build OPTIONS.  Redirect build
output to LOG.  On success, a path to a tarball in the store is returned.  The
tarball contains a bundle of the team's work.  On failure, #f is returned."

  (define work-name
    (lookup-work-name/id (team-wid team)))

  (dynamic-wind
    (lambda () (format log "=== BEGIN GUIX ===\n"))
    (lambda () (catch #t
            (lambda ()
              (parameterize ((current-build-output-port log)
                             (current-output-port log)
                             (current-error-port log))
                (let ((generator (assoc-ref (unbox *package-generators*) work-name)))
                  (receive (base-package branch)
                      (generator (team-variant team))
                    (let* ((package-variant (package->package-variant base-package branch))
                           (patches (assoc-ref (unbox *package-patches*) work-name))
                           (package  (package->package-patched
                                      package-variant
                                      (map (lambda (patch)
                                             (cons (archive-ref archive patch) patch))
                                           patches))))
                      (run-with-store (open-connection)
                        (apply build-team-work% package options)))))))
            (lambda (key . args)
              (log-msg 'WARNING "While building team "
                       (team-token team)
                       " work: " key args)
              #f)))
    (lambda () (format log "===  END GUIX  ===\n"))))

(define (get-package-source package)
  (run-with-store (open-connection)
    (mlet %store-monad ((source (lower-object (package-source package))))
      (return source))))

(define (get-team-work team)
  "Return a path to a tarball with the work for TEAM.  The path must be deleted by
the caller to avoid leak."
  (let* ((work-name (lookup-work-name/id (team-wid team)))
         (generator (assoc-ref (unbox *package-generators*) work-name)))
    (receive (base-package branch)
        (generator (team-variant team))
      (parameterize ((current-output-port (%make-void-port "w"))
                     (current-error-port (%make-void-port "w")))
        (let* ((src (get-package-source
                     (package->package-variant base-package branch)))
               (port (mkstemp "/tmp/handout-team-work-XXXXXX"))
               (dst (port-filename port)))
          (close-port port)
          (if (directory-exists? src)
              (compress-directory-to src dst)
              (copy-file src dst))
          (log-msg 'INFO
                   "work of team " (team-token team)
                   " generated at " dst " from " src)
          dst)))))
