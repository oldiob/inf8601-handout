(define-module (handout db)
  #:use-module (ice-9 textual-ports)
  #:use-module (sqlite3)
  #:use-module (system foreign)
  #:export-syntax (query*
                   with-db))


(define current-connection (make-parameter #f))

(define (open-connection uri)
  (sqlite-open uri (logior SQLITE_OPEN_CREATE SQLITE_OPEN_READWRITE)))

(define (init-db% db)
  (and
   (sqlite-exec db
                (call-with-input-file "data/schema.sql"
                  get-string-all))
   db))

(define (open-db uri)
  (and=> (open-connection uri) init-db%))

(define (close-db db)
  (sqlite-close db))

(define (sqlite-query* db transform sql . bindings)
  (let ((stmt (sqlite-prepare db sql)))
    (for-each (lambda (key+value)
                (sqlite-bind stmt
                             (car key+value)
                             (cdr key+value)))
              bindings)
    (let ((result (sqlite-map transform stmt)))
      (sqlite-finalize stmt)
      result)))

(define-syntax query*
  (syntax-rules ()
    ((_ fmt (key value) ...)
     (query* identity fmt (key value) ...))
    ((_ transformer fmt (key value) ...)
     (sqlite-query* (current-connection) transformer fmt
                    `(key . ,value) ...))))

(define-syntax-rule (with-db uri body body* ...)
  (dynamic-wind
    (lambda ()
      (current-connection (open-db uri)))
    (lambda () body body* ...)
    (lambda ()
      (and=> (current-connection #f) close-db))))
