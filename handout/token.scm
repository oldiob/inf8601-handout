(define-module (handout token)
  #:use-module ((gcrypt random) #:prefix crypt:)
  #:use-module (srfi srfi-9)
  #:export (make-token))

(define (make-token)
  (crypt:random-token))
