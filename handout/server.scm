(define-module (handout server)
  #:use-module (ice-9 atomic)
  #:use-module (ice-9 exceptions)
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:use-module (ice-9 textual-ports)
  #:use-module (ice-9 threads)
  #:use-module (handout db)
  #:use-module (handout archive)
  #:use-module (handout build)
  #:use-module (handout db)
  #:use-module (handout exceptions)
  #:use-module (handout schedule)
  #:use-module (handout team)
  #:use-module (handout work)
  #:use-module (logging logger)
  #:use-module (logging rotating-log)
  #:use-module (logging port-log)
  #:use-module (oop goops)
  #:use-module (srfi srfi-19)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-34)
  #:use-module (sqlite3)
  #:export (run-server
            spawn-server))

(define current-client-id (make-parameter #f))

(define new-client-id
  (let ((counter (make-atomic-box 0)))
    (lambda ()
      (let* ((old (atomic-box-ref counter))
             (new (1+ old)))
        (if (= old (atomic-box-compare-and-swap! counter old new))
            (current-client-id new)
            (new-client-id))))))

(define SQLITE_CONSTRAINT_FOREIGNKEY
  (logior SQLITE_CONSTRAINT (ash 3 8)))

(define SQLITE_CONSTRAINT_NOTNULL
  (logior SQLITE_CONSTRAINT (ash 5 8)))

(define (unix-epoch->date-string epoch)
  (date->string (time-utc->date (make-time time-utc 0 epoch))))

(define (format-work work)
  (format #f "\
name: ~a
scheduled: ~a
deadline: ~a
"
          (work-name work)
          (unix-epoch->date-string (work-begin-epoch work))
          (unix-epoch->date-string (work-end-epoch work))))

;;; HIGH LEVEL
(define (list-work client)
  (let ((result (string-join (map format-work (list-works)) "\n" 'infix)))
    (put-string client "print\n")
    (put-string client result)))

(define (status client token)
  (let ((team (lookup-team/token token)))
    (if (team? team)
        (format client "print
attempt: ~a
best-result: ~a%
last-result: ~a%
"
                (team-attempt team)
                (team-best-grade team)
                (team-last-grade team))
        (format client "print
No team with token: ~a
" token))))

(define (fetch client token)

  (define team (lookup-team/token token))

  (if team
      (let* ((tgz (get-team-work team))
             (port (open-input-file tgz))
             (size (stat:size (stat port #t))))
        (format client "recvfile ~a.tar.gz ~a\n"
                (lookup-work-name/id (team-wid team))
                size)
        (sendfile client port size)
        (close-port port)
        (delete-file tgz))
      (format client "print
No team with token: ~a
" token)))

(define (submit% client team size)
  (let* ((port (mkstemp "/tmp/handout-user-tgz-XXXXXX"))
         (user-tgz (port-filename port)))
    (dynamic-wind
      (lambda ()
        (sendfile port client size)
        (close-port port))
      (lambda ()
        (format client "Sending work for build ...\n")
        (match (with-archive user-tgz
                 (cut build-team-work <> team client))
          ((? string? path)
           (format client "Build successful.\n")
           (schedule-computation team path)
           (format client "Schedule successful.\n")
           (computation-status client team))
          (#f
           (format client "Failed to build work.\n"))))
      (lambda ()
        (delete-file user-tgz)))))

(define (submit client token size)

  (define team (lookup-team/token token))

  (format client "print\n")

  (cond
   ((and team size)
    (submit% client team size))
   ((not size)
    (format client "Invalid size of archive\n"))
   (else (format client "No team with token: ~a\n" token))))

(define* (teamup client tp sid1 #:optional (sid2 sid1))

  (define wid (lookup-work-id/name tp))

  (put-string client "print\n")
  (catch 'sqlite-error
    (lambda ()
      (if wid
          (let ((team (or (lookup-team sid1 sid2 wid) (make-team sid1 sid2 wid))))
            (format client "Here's your team token: ~a\n" (team-token team)))
          (format client "Invalid work `~a'\n" tp)))
    (lambda (key . args)
      (match args
        ((who code msg)
         (cond
          ((= code SQLITE_CONSTRAINT_FOREIGNKEY)
           (put-string client "Invalid student or work\n"))
          ((= code SQLITE_CONSTRAINT_UNIQUE)
           (put-string client "Student already assigned to a team for this work\n"))
          ((= code SQLITE_CONSTRAINT_NOTNULL)
           (put-string client "Bad inputs.\n"))
          (else
           (put-string client "Unknown error.  Contact the administrator.\n"))))
        (_
         (put-string client "Unknown SQL exception\n"))))))

;;; LOW LEVEL
(define (recv-request client)
  (let ((line (get-line client)))
    (if (string? line)
        (string-split line #\space)
        #f)))

(define (handle-client client)
  (guard (ex ((handout-client-exception? ex)
              (put-string client (exception-message ex)))
             (else
              (log-msg 'ERROR "Exception "
                       (exception-kind ex) ": "
                       (exception-args ex))
              #f))
    (let ((request (recv-request client)))
        (log-msg 'INFO "Request: " request)
        (match request
          (("list")
           (list-work client))
          (("fetch" token)
           (fetch client token))
          (("submit" token size)
           (submit client token (string->number size)))
          (("status" token)
           (status client token))
          (("teamup" work sid1)
           (teamup client work (string->number sid1)))
          (("teamup" work sid1 sid2)
           (teamup client work
                   (string->number sid1)
                   (string->number sid2)))
          (x
           (format client "print
Invalid request: ~a\n" (string-join x " " 'infix)))))))

(define (client-address details)
  (let ((family (sockaddr:fam details)))
    (cond
     ((= family AF_UNIX) (sockaddr:path details))
     ((or (= family AF_INET)
          (= family AF_INET6))
      (inet-ntop (sockaddr:fam details)
                 (sockaddr:addr details)))
     (else "<unknown>"))))

(define (on-accept handle-client client details)
  (begin-thread
   (dynamic-wind
     new-client-id
     (lambda ()
       (log-msg 'INFO "New client: " (client-address details))
       (handle-client client))
     (lambda ()
       (shutdown client 2)
       (close-port client)))))

(define (log-formatter level time msg)
  (format #f "~a (~a) [~a]: ~a\n"
          (unix-epoch->date-string time)
          level
          (current-client-id)
          msg))

(define (setup-logging)
  (let ((logger (make <logger>))
        (rotate (make <rotating-log>
                  #:file-name "handout-server.log"
                  #:formatter log-formatter))
        (console (make <port-log>
                   #:port (current-error-port)
                   #:formatter log-formatter)))
    (add-handler! logger rotate)
    (add-handler! logger console)
    (set-default-logger! logger)
    (open-log! logger)))

(define (shutdown-logging)
  (flush-log)
  (close-log!)
  (set-default-logger! #f))

(define (run-server port)
  (setup-logging)
  (let ((sock (socket PF_INET SOCK_STREAM 0)))
    (setsockopt sock SOL_SOCKET SO_REUSEPORT 1)
    (bind sock AF_INET INADDR_ANY port)
    (listen sock 5)
    (dynamic-wind
      (lambda ()
        (setup-logging)
        (let ((sockaddr (getsockname sock)))
          (log-msg 'INFO "Listening on "
                   (inet-ntop AF_INET (sockaddr:addr sockaddr))
                   ":"
                   (sockaddr:port sockaddr))))
      (lambda ()
        (let loop ()
          (match (accept sock)
            ((client . details)
             (on-accept handle-client client details))
            (x (log-msg 'ERROR
                        "Error while accepting client: " x)))
          (loop)))
      (lambda ()
        (close-port sock)
        (shutdown-logging)))))

(define (spawn-server . args)
  (begin-thread
   (apply run-server args)))
