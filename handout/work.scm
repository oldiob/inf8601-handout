(define-module (handout work)
  #:use-module (handout db)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-9)
  #:export (drop-all-works
            drop-work/name
            list-works
            lookup-work-id/name
            lookup-work-name/id
            make-work
            work?
            work-begin-epoch
            work-end-epoch
            work-name))

(define-record-type <work>
  (make-work% name begin-epoch end-epoch)
  work?
  (name work-name)
  (begin-epoch work-begin-epoch)
  (end-epoch work-end-epoch))

(define (vector->work vec)
  (apply make-work% (vector->list vec)))

(define (make-work name begin-epoch end-epoch)
  (let ((work (make-work% name begin-epoch end-epoch)))
    (query* "
INSERT INTO work (name, begin_epoch, end_epoch)
VALUES(:name, :begin, :end)"
            (name (work-name work))
            (begin (work-begin-epoch work))
            (end (work-end-epoch work)))
    work))

(define (lookup-work-name/id id)
  (match (query* "SELECT name FROM work WHERE id = :id"
                 (id id))
    ((#(x)) x)
    (_ #f)))

(define (lookup-work-id/name name)
  (match (query* "SELECT id FROM work WHERE name = :name" (name name))
    ((#(x)) x)
    (_ #f)))

(define (drop-all-works)
  (query* "DELETE FROM work"))

(define (drop-work/name name)
  (query* "DELETE FROM work WHERE name = :name" (name name)))

(define (list-works)
  (query* vector->work "SELECT name, begin_epoch, end_epoch FROM work"))
