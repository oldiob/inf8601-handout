(define-module (handout archive)
  #:use-module (guix build utils)
  #:use-module (ice-9 control)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-26)
  #:export (archive?
            archive-path
            archive-ref
            compress-directory-to
            drop-archive
            list-archive
            make-archive)
  #:export-syntax (with-archive))

(define-record-type <archive>
  (make-archive% path)
  archive?
  (path archive-path set-archive-path!))

(define-syntax-rule (with-archive tgz proc)
  (let ((archive #f))
    (dynamic-wind
      (lambda () (set! archive (make-archive tgz)))
      (lambda () (proc archive))
      (lambda () (drop-archive archive) (set! archive #f)))))

(define (make-archive tgz)
  (let* ((archive (make-archive% (mkdtemp "/tmp/handout-archive-XXXXXX")))
         (tgz (canonicalize-path tgz))
         (status (system*
                  "tar"
                  "--extract"
                  "--file"
                  tgz
                  "--directory"
                  (archive-path archive))))
    (if (and=> (status:exit-val status) (cut = <> EXIT_SUCCESS))
        archive
        (begin (drop-archive archive) #f))))

(define (drop-archive archive)
  (if(archive? archive)
     (begin
       (archive-path archive)
       (delete-file-recursively (archive-path archive))
       (set-archive-path! archive #f)
       #t)
     #f))

(define (list-archive archive)
  (and (archive? archive)
       (find-files (archive-path archive) (const #t))))

(define (archive-ref archive key)
  (if (archive? archive)
      (let* ((dir (archive-path archive))
             (key (string-append dir "/" key)))
        (let/ec return
          (find-files dir
                      (lambda (file _)
                        (when (string=? key file)
                          (return file))))
          #f))
      #f))

(define (compress-directory-to directory target)
  (and=> (status:exit-val
          (system* "tar"
                   "--mode=a+rwX"
                   "--create"
                   "--gzip"
                   "--file"
                   target
                   "--directory"
                   directory
                   "."))
         zero?))
