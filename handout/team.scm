(define-module (handout team)
  #:use-module (ice-9 match)
  #:use-module (handout db)
  #:use-module (handout token)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-27)
  #:export (drop-all-teams
            drop-team-sceduled
            drop-team/token
            list-teams
            lookup-team
            lookup-team/token
            make-team
            <team>
            team?
            team-attempt
            team-best-grade
            team-last-grade
            team-sid1
            team-sid2
            team-token
            team-variant
            team-wid
            update-team-grade))

(define-record-type <team>
  (make-team% token sid1 sid2 wid variant best-grade last-grade attempt)
  team?
  (token team-token)
  (sid1 team-sid1)
  (sid2 team-sid2)
  (wid team-wid)
  (variant team-variant)
  (best-grade team-best-grade)
  (last-grade team-last-grade)
  (attempt team-attempt))

(define (vector->team vec)
  (apply make-team% (vector->list vec)))

(define make-team
  (match-lambda*
    ((sid1 wid)
     (make-team sid1 sid1 wid))
    ((sid1 sid2 wid)
     (let* ((team (make-team% (make-token) sid1 sid2 wid (random-integer 10000) 0 0 0)))
       (query* "
INSERT INTO team (token, student1_id, student2_id, work_id, variant)
VALUES(:token, :sid1, :sid2, :wid, :variant)"
               (token (team-token team))
               (sid1 (team-sid1 team))
               (sid2 (team-sid2 team))
               (wid (team-wid team))
               (variant (team-variant team)))
       team))))

(define (drop-all-teams)
  (query* "DELETE FROM team"))

(define (drop-team/token token)
  (query* "DELETE FROM team WHERE token = :token" (token token)))

(define (list-teams)
  (query* vector->team "SELECT * FROM team"))

(define (lookup-team sid1 sid2 wid)
  (match (query* vector->team "
SELECT *
FROM team
WHERE student1_id = :sid1
AND student2_id = :sid2
AND work_id = :wid"
                 (sid1 sid1)
                 (sid2 sid2)
                 (wid wid))
    ((x) x)
    (_ #f)))

(define (lookup-team/token token)
  (match (query* vector->team "
SELECT *
FROM team
WHERE token = :token
"
                 (token token))
    ((x) x)
    (_ #f)))

(define (update-team-grade token grade)
  (query* "
UPDATE team
SET best_grade = MAX(best_grade, :grade),
    last_grade = :grade,
    attempt    = attempt + 1
WHERE token = :token"
          (grade grade)
          (token token)))
