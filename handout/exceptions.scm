(define-module (handout exceptions)
  #:use-module (ice-9 exceptions)
  #:export (make-handout-client-exception
            handout-client-exception?
            &handout-client-exception))

(define-exception-type &handout-client-exception &message
  make-handout-client-exception
  handout-client-exception?)
