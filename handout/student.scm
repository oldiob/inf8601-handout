(define-module (handout student)
  #:use-module (ice-9 match)
  #:use-module (handout db)
  #:use-module (srfi srfi-9)
  #:export (drop-all-students
            drop-student/id
            drop-student/mail
            make-student
            list-students
            lookup-student/id
            lookup-student/mail
            student?
            student-id
            student-mail))

(define-record-type <student>
  (make-student% id mail)
  student?
  (id student-id)
  (mail student-mail))

(define (vector->student vec)
  (apply make-student% (vector->list vec)))

(define (make-student id mail)
  (let ((student (make-student% id mail)))
    (query* "INSERT INTO student (id, mail) VALUES(:id, :mail)"
            (id (student-id student))
            (mail (student-mail student)))
    student))

(define (drop-student/id id)
  (query* "DELETE FROM student WHERE id = :id" (id id)))

(define (drop-student/mail mail)
  (query* "DELETE FROM student WHERE mail = :mail" (mail mail)))

(define (drop-all-students)
  (query* "DELETE FROM student"))

(define (list-students)
  (query* vector->student "SELECT * FROM student"))

(define (lookup-student/id id)
  (match (query* vector->student "SELECT * FROM student WHERE id = :id"
                 (id id))
    ((x) x)
    (_ #f)))

(define (lookup-student/mail mail)
  (match
      (query* vector->student "SELECT * FROM student WHERE mail = :mail"
              (mail mail))
    ((x) x)
    (_ #f)))
