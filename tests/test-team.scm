(define-module (tests test-student)
  #:use-module (handout student)
  #:use-module (handout team)
  #:use-module (handout work)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-64)
  #:use-module (tests common))


(test-begin "team")

(test-assert "make-team"
  (with-temp-db
   (make-student 1 "1")
   (make-student 2 "2")
   (make-student 3 "3")
   (make-student 4 "4")
   (make-student 5 "5")
   (make-student 6 "6")
   (make-work "" 0 1)
   (and
    (team? (make-team 1 1))
    (team? (make-team 2 1))
    (team? (make-team 3 1))
    (team? (make-team 4 1))
    (team? (make-team 5 6 1)))))

(test-assert "list-teams"
  (with-temp-db
   (make-work "" 0 1)
   (make-student 1 "1")
   (make-student 2 "2")
   (make-team 1 1)
   (make-team 2 1)
   (= 2 (length (list-teams)))))

(test-assert "Student with itself"
  (with-temp-db
   (make-student 1 "1")
   (make-work "" 0 1)
   (make-team 1 1 1)))

(test-assert "Keep last grade"
  (with-temp-db
   (make-student 1 "1")
   (make-work "" 0 1)
   (let ((token (team-token (make-team 1 1))))
     (and (zero? (team-last-grade (lookup-team/token token)))
          (update-team-grade token 10)
          (= 10 (team-best-grade (lookup-team/token token)))
          (= 10 (team-last-grade (lookup-team/token token)))
          (update-team-grade token 0)
          (= 10 (team-best-grade (lookup-team/token token)))
          (= 0 (team-last-grade (lookup-team/token token)))))))

(test-assert "Grade only goes up"
  (with-temp-db
   (make-student 1 "1")
   (make-work "" 0 1)
   (let ((token (team-token (make-team 1 1))))
     (and (zero? (team-best-grade (lookup-team/token token)))
          (update-team-grade token 10)
          (= 10 (team-best-grade (lookup-team/token token)))
          (update-team-grade token 50)
          (= 50 (team-best-grade (lookup-team/token token)))
          (update-team-grade token 0)
          (= 50 (team-best-grade (lookup-team/token token)))
          (update-team-grade token 100)
          (= 100 (team-best-grade (lookup-team/token token)))))))

(test-group "constraints"
  (test-constraint "Student does not exist"
    (with-temp-db
     (make-work "" 0 1)
     (make-team 0 1)))
  (test-constraint "Student in two team"
    (with-temp-db
     (make-student 1 "1")
     (make-student 2 "2")
     (make-student 3 "3")
     (make-work "" 0 1)
     (make-team 1 2 1)
     (make-team 1 3 1)))
  (test-constraint "Late team"
    (with-temp-db
     (make-student 1 "1")
     (make-student 2 "2")
     (make-work "" 0 1)
     (make-team 1 1)
     (make-team 2 1 1)))
  (test-constraint "Maximum grade is 100"
    (with-temp-db
     (make-student 1 "1")
     (make-work "" 0 1)
     (let ((token (team-token (make-team 1 1))))
       (update-team-grade token 101)))))

(test-end "team")
