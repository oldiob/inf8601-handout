(define-module (tests test-student)
  #:use-module (handout work)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-64)
  #:use-module (tests common))

(define (make-foo-and-bar)
  (and (work? (make-work "foo" 0 1))
       (work? (make-work "bar" 0 1))))

(test-begin "work")

(test-assert "make-work"
  (with-temp-db
   (make-foo-and-bar)))

(test-assert "list-works"
  (with-temp-db
   (and (make-foo-and-bar)
        (= 2 (length (list-works))))))

(test-assert "drop-work/name"
  (with-temp-db
   (let ((name (work-name (make-work "foo" 0 1))))
     (and (= 1 (length (list-works)))
          (drop-work/name name)
          (= 0 (length (list-works)))))))

(test-assert "drop-all-works"
  (with-temp-db
   (and (make-foo-and-bar)
        (= 2 (length (list-works)))
        (drop-all-works)
        (= 0 (length (list-works))))))

(test-group "constraints"
  (test-constraint "work same name"
    (with-temp-db
     (make-work "foo" 0 1)
     (make-work "foo" 0 1)))
  (test-constraint "end epoch before begin epoch"
    (with-temp-db
     (make-work "foo" 1 0))))

(test-end "work")
