(define-module (tests test-student)
  #:use-module (handout student)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-64)
  #:use-module (tests common))

(test-begin "student")

(test-assert "make-student"
  (with-temp-db
   (and
    (student? (make-student 0 "foo"))
    (student? (make-student 1 "bar")))))

(test-assert "list-students"
  (with-temp-db
   (make-student 0 "foo")
   (make-student 1 "bar")
   (= 2 (length (filter student? (list-students))))))

(test-assert "lookup-student/id"
  (with-temp-db
   (and (not (lookup-student/id 0))
        (make-student 0 "foo")
        (student? (lookup-student/id 0)))))

(test-assert "lookup-student/mail"
  (with-temp-db
   (and (not (lookup-student/mail "foo"))
        (make-student 0 "foo")
        (student? (lookup-student/mail "foo")))))

(test-assert "drop-student/id"
  (with-temp-db
   (let* ((foo (make-student 0 "foo"))
          (id (student-id foo)))
     (and (student? (lookup-student/id id))
          (drop-student/id id)
          (not (lookup-student/id id))))))

(test-assert "drop-student/mail"
  (with-temp-db
   (let* ((foo (make-student 0 "foo"))
          (mail (student-mail foo)))
     (and (student? (lookup-student/mail mail))
          (drop-student/mail mail)
          (not (lookup-student/mail mail))))))

(test-assert "drop-all-students"
  (with-temp-db
   (let ((foo (make-student 0 "foo"))
         (bar (make-student 1 "bar")))
     (and
      (= 2 (length (filter student? (list-students))))
      (drop-all-students)
      (= 0 (length (list-students)))))))

(test-group "contraints"
  (test-constraint "student same id"
    (with-temp-db
     (make-student 0 "foo")
     (make-student 0 "bar")))
  (test-constraint "student same mail"
    (with-temp-db
     (make-student 0 "foo")
     (make-student 1 "foo"))))

(test-end "student")
