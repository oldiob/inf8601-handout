(define-module (tests common)
  #:use-module (ice-9 match)
  #:use-module (handout db)
  #:use-module (srfi srfi-64)
  #:use-module (sqlite3)
  #:export-syntax (test-constraint
                   with-temp-db))

(define-syntax-rule (with-temp-db body body* ...)
  (let ((tmp (port-filename (mkstemp "/tmp/inf8601-test-db-XXXXXX"))))
    (dynamic-wind
      (const #f)
      (lambda ()
        (with-db tmp body body* ...))
      (lambda ()
        (delete-file tmp)))))

(define-syntax-rule (test-constraint name test-expr)
  (test-assert name
    (catch 'sqlite-error
      (lambda () test-expr #f)
      (lambda (key . args)
        (match args
          ((_ code . rest)
           (= SQLITE_CONSTRAINT (logand SQLITE_CONSTRAINT code)))
          (_ #f))))))
