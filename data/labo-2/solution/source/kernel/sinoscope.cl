#include "helpers.cl"

/*
 * TODO
 *
 * Déclarez les structures partagées avec l'autre code si nécessaire selon votre énoncé.
 */

typedef struct __attribute__((packed)) int_args {
    int width;
    int height;
    int taylor;
    int interval;
} int_args_t;

typedef struct __attribute__((packed)) float_args {
    float interval_inverse;
    float time;
    float max;
    float phase0;
    float phase1;
    float dx;
    float dy;
} float_args_t;

/*
 * TODO
 *
 * Modifiez les paramètres du noyau avec ceux demandés par votre énoncé.
 */

__kernel void sinoscope_kernel(__global unsigned char* buffer, int_args_t int_args, float_args_t float_args) {
    /*
     * TODO
     *
     * En vous basant sur l'implémentation dans `sinoscope-serial.c`, effectuez le même calcul. Ce
     * noyau est appelé pour chaque coordonnée, alors vous ne devez pas spécifiez les boucles
     * extérieures.
     */

    int j = get_global_id(0) / int_args.width;
    int i = get_global_id(0) - (j * int_args.width);

    float px    = float_args.dx * j - 2 * M_PI;
    float py    = float_args.dy * i - 2 * M_PI;
    float value = 0;

    for (int k = 1; k <= int_args.taylor; k += 2) {
        value += sin(px * k * float_args.phase1 + float_args.time) / k;
        value += cos(py * k * float_args.phase0) / k;
    }

    value = (atan(value) - atan(-value)) / M_PI;
    value = (value + 1) * 100;

    pixel_t pixel;
    color_value(&pixel, value, int_args.interval, float_args.interval_inverse);

    int index = (i * 3) + (j * 3) * int_args.width;

    buffer[index + 0] = pixel.bytes[0];
    buffer[index + 1] = pixel.bytes[1];
    buffer[index + 2] = pixel.bytes[2];
}
