#include "log.h"
#include "sinoscope.h"

int sinoscope_opencl_init(sinoscope_opencl_t* opencl, cl_device_id opencl_device_id, unsigned int width,
                          unsigned int height) {
    /*
     * TODO
     *
     * Initialiser `opencl->context` du périphérique reçu en paramètre.
     *
     * Initialiser `opencl->queue` à partir du contexte précèdent.
     *
     * Initialiser `opencl->buffer` à partir du context précèdent (width * height * 3).
     *
     * Initialiser `opencl->kernel` à partir du contexte et du périphérique reçu en
     * paramètre. Utilisez la fonction `opencl_load_kernel_code` déclaré dans `opencl.h` pour lire
     * le code du noyau OpenCL `kernel/sinoscope.cl` dans un tampon. Compiler le noyau
     * en ajoutant le paramètre `"-I " __OPENCL_INCLUDE__`.
     */
    cl_int status;

    opencl->context = clCreateContext(NULL, 1, &opencl_device_id, NULL, NULL, &status);
    if (status != CL_SUCCESS) {
        LOG_ERROR("clCreateContext (%d)", status);
        goto fail_exit;
    }

    opencl->queue = clCreateCommandQueueWithProperties(opencl->context, opencl_device_id, NULL, &status);
    if (status != CL_SUCCESS) {
        LOG_ERROR("clCreateCommandQueueWithProperties (%d)", status);
        goto fail_exit;
    }

    unsigned int buffer_size = width * height * 3;
    opencl->buffer           = clCreateBuffer(opencl->context, CL_MEM_WRITE_ONLY, buffer_size, NULL, &status);
    if (status != CL_SUCCESS) {
        LOG_ERROR("clCreateBuffer (%d)", status);
        goto fail_exit;
    }

    char* code;
    size_t code_len;
    if (opencl_load_kernel_code(&code, &code_len) < 0) {
        LOG_ERROR("failed to load opencl kernel code");
        goto fail_exit;
    }

    cl_program opencl_program = clCreateProgramWithSource(opencl->context, 1, (const char**)&code, &code_len, &status);
    free(code);
    if (status != CL_SUCCESS) {
        LOG_ERROR("clCreateProgramWithSource (%d)", status);
        goto fail_exit;
    }

    status = clBuildProgram(opencl_program, 1, &opencl_device_id, "-I " __OPENCL_INCLUDE__, NULL, NULL);
    if (status != CL_SUCCESS) {
        LOG_ERROR("clBuildProgram (%d)", status);
        opencl_print_build_log(opencl_program, opencl_device_id);
        goto fail_exit;
    }

    opencl->kernel = clCreateKernel(opencl_program, "sinoscope_kernel", &status);
    if (status != CL_SUCCESS) {
        LOG_ERROR("clCreateKernel (%d)", status);
        goto fail_exit;
    }

    status = clReleaseProgram(opencl_program);
    if (status != CL_SUCCESS) {
        LOG_ERROR("clReleaseProgram (%d)", status);
    }

    return 0;

fail_exit:
    return -1;
}

void sinoscope_opencl_cleanup(sinoscope_opencl_t* opencl) {
    /*
     * TODO
     *
     * Libérez les ressources associées à `opencl->context`, `opencl->queue`,
     * `opencl->buffer` et `opencl->kernel`.
     */

    cl_int status = clReleaseKernel(opencl->kernel);
    if (status != CL_SUCCESS) {
        LOG_ERROR("clReleaseKernel (%d)", status);
    }

    status = clReleaseCommandQueue(opencl->queue);
    if (status != CL_SUCCESS) {
        LOG_ERROR("clReleaseCommandQueue (%d)", status);
    }

    status = clReleaseMemObject(opencl->buffer);
    if (status != CL_SUCCESS) {
        LOG_ERROR("clReleaseMemObject (%d)", status);
    }

    status = clReleaseContext(opencl->context);
    if (status != CL_SUCCESS) {
        LOG_ERROR("clReleaseContext (%d)", status);
    }
}

/*
 * TODO
 *
 * Déclarez les structures partagées avec le noyau OpenCL si nécessaire selon votre énoncé.
 */

typedef struct __attribute__((packed)) int_args {
    int width;
    int height;
    int taylor;
    int interval;
} int_args_t;

typedef struct __attribute__((packed)) float_args {
    float interval_inverse;
    float time;
    float max;
    float phase0;
    float phase1;
    float dx;
    float dy;
} float_args_t;

int sinoscope_image_opencl(sinoscope_t* sinoscope) {
    if (sinoscope->opencl == NULL) {
        LOG_ERROR_NULL_PTR();
        goto fail_exit;
    }

    /*
     * TODO
     *
     * Configurez les paramètres du noyau OpenCL afin d'envoyer `sinoscope->opencl->buffer` et les
     * autres données nécessaire à son exécution.
     *
     * Démarrez le noyau OpenCL et attendez son exécution.
     *
     * Effectuez la lecture du tampon `sinoscope->opencl->buffer` contenant le résultat dans
     * `sinoscope->buffer`.
     */

    int_args_t int_args = {
        .width    = sinoscope->width,
        .height   = sinoscope->height,
        .taylor   = sinoscope->taylor,
        .interval = sinoscope->interval,
    };

    float_args_t float_args = {
        .interval_inverse = sinoscope->interval_inverse,
        .time             = sinoscope->time,
        .max              = sinoscope->max,
        .phase0           = sinoscope->phase0,
        .phase1           = sinoscope->phase1,
        .dx               = sinoscope->dx,
        .dy               = sinoscope->dy,
    };

    cl_int status =
        clSetKernelArg(sinoscope->opencl->kernel, 0, sizeof(sinoscope->opencl->buffer), &sinoscope->opencl->buffer);
    if (status != CL_SUCCESS) {
        LOG_ERROR("clSetKernelArg (%d)", status);
        goto fail_exit;
    }

    status = clSetKernelArg(sinoscope->opencl->kernel, 1, sizeof(int_args), &int_args);
    if (status != CL_SUCCESS) {
        LOG_ERROR("clSetKernelArg (%d)", status);
        goto fail_exit;
    }

    status = clSetKernelArg(sinoscope->opencl->kernel, 2, sizeof(float_args), &float_args);
    if (status != CL_SUCCESS) {
        LOG_ERROR("clSetKernelArg (%d)", status);
        goto fail_exit;
    }

    size_t work_size = sinoscope->width * sinoscope->height;

    status = clEnqueueNDRangeKernel(sinoscope->opencl->queue, sinoscope->opencl->kernel, 1, NULL, &work_size, NULL, 0,
                                    NULL, NULL);
    if (status != CL_SUCCESS) {
        LOG_ERROR("clEnqueueNDRangeKernel (%d)", status);
        goto fail_exit;
    }

    status = clFinish(sinoscope->opencl->queue);
    if (status != CL_SUCCESS) {
        LOG_ERROR("clFinish (%d)", status);
        goto fail_exit;
    }

    status = clEnqueueReadBuffer(sinoscope->opencl->queue, sinoscope->opencl->buffer, CL_TRUE, 0,
                                 sinoscope->buffer_size, sinoscope->buffer, 0, NULL, NULL);
    if (status != CL_SUCCESS) {
        LOG_ERROR("clEnqueueReadBuffer (%d)", status);
        goto fail_exit;
    }

    return 0;

fail_exit:
    return -1;
}
