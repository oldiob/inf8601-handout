PRAGMA foreign_keys = ON;

CREATE TABLE IF NOT EXISTS student (
id    INTEGER PRIMARY KEY,
mail  VARCHAR(512),

UNIQUE(mail)
);

CREATE TABLE IF NOT EXISTS work (
id          INTEGER PRIMARY KEY AUTOINCREMENT,
name        CHAR(128),
begin_epoch INTEGER,
end_epoch   INTEGER,

CHECK(begin_epoch < end_epoch),
UNIQUE(name)
);

CREATE TABLE IF NOT EXISTS team (
token       CHAR(40) PRIMARY KEY,
student1_id INTEGER NOT NULL,
student2_id INTEGER NOT NULL,
work_id     INTEGER,
variant     INTEGER,
best_grade  REAL DEFAULT 0 CHECK(0 <= best_grade) CHECK(best_grade <= 100),
last_grade  REAL DEFAULT 0 CHECK(0 <= last_grade) CHECK(last_grade <= 100),
attempt     INTEGER DEFAULT 0 CHECK(0 <= attempt),

UNIQUE(student1_id, work_id),
UNIQUE(student2_id, work_id),

FOREIGN KEY(student1_id) REFERENCES student(id),
FOREIGN KEY(student2_id) REFERENCES student(id),
FOREIGN KEY(work_id)     REFERENCES work(id)
);

CREATE TABLE IF NOT EXISTS schedule (
slurm_id   INTEGER PRIMARY KEY,
team_token INTEGER NOT NULL,
tarball    VARCHAR(1024),

UNIQUE(team_token),

FOREIGN KEY(team_token) REFERENCES team(token)
);
