#include <assert.h>
#include <stddef.h>

#include "heatsim.h"
#include "log.h"

int heatsim_init(heatsim_t* heatsim, unsigned int dim_x, unsigned int dim_y) {
    /*
     * TODO: Initialiser tous les membres de la structure `heatsim`.
     *       Le communicateur doit être périodique. Le communicateur
     *       cartésien est périodique en X et Y.
     */

    int status = MPI_Comm_size(MPI_COMM_WORLD, &heatsim->rank_count);
    if (status != MPI_SUCCESS) {
        LOG_ERROR_MPI("MPI_Comm_size", status);
        goto fail_exit;
    }

    if (heatsim->rank_count != dim_x * dim_y) {
        LOG_ERROR("mismatch between block and process count, rank_count=%d, dim_x=%d, dim_y=%d", heatsim->rank_count,
                  dim_x, dim_y);
        goto fail_exit;
    }

    status = MPI_Comm_rank(MPI_COMM_WORLD, &heatsim->rank);
    if (status != MPI_SUCCESS) {
        LOG_ERROR_MPI("MPI_Comm_rank", status);
        goto fail_exit;
    }

    int dims[2]     = {dim_x, dim_y};
    int periodic[2] = {1, 1};

    status = MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periodic, 0, &heatsim->communicator);
    if (status != MPI_SUCCESS) {
        LOG_ERROR_MPI("MPI_Cart_create", status);
        goto fail_exit;
    }

    status = MPI_Cart_shift(heatsim->communicator, 0, 1, &heatsim->rank_west_peer, &heatsim->rank_east_peer);
    if (status != MPI_SUCCESS) {
        LOG_ERROR_MPI("MPI_Cart_shift", status);
        goto fail_exit;
    }

    status = MPI_Cart_shift(heatsim->communicator, 1, 1, &heatsim->rank_north_peer, &heatsim->rank_south_peer);
    if (status != MPI_SUCCESS) {
        LOG_ERROR_MPI("MPI_Cart_shift", status);
        goto fail_exit;
    }

    status = MPI_Cart_coords(heatsim->communicator, heatsim->rank, 2, heatsim->coordinates);
    if (status != MPI_SUCCESS) {
        LOG_ERROR_MPI("MPI_Cart_coords", status);
        goto fail_exit;
    }

    return 0;

fail_exit:
    return -1;
}

static int setup_mpi_type_grid_struct(MPI_Datatype* grid_struct) {
    int blocklengths[3]       = {1, 1, 1};
    MPI_Aint displacements[3] = {offsetof(grid_t, width), offsetof(grid_t, height), offsetof(grid_t, padding)};
    MPI_Datatype types[3]     = {MPI_UNSIGNED, MPI_UNSIGNED, MPI_UNSIGNED};

    int status = MPI_Type_create_struct(3, blocklengths, displacements, types, grid_struct);
    if (status != MPI_SUCCESS) {
        LOG_ERROR_MPI("MPI_Type_struct", status);
        goto fail_exit;
    }

    status = MPI_Type_commit(grid_struct);
    if (status != MPI_SUCCESS) {
        LOG_ERROR_MPI("MPI_Type_commit", status);
        goto fail_exit;
    }

    return 0;

fail_exit:
    return -1;
}

static int setup_mpi_type_grid_data(MPI_Datatype* grid_data, grid_t* grid) {
    int status = MPI_Type_contiguous(grid->width_padded * grid->height_padded, MPI_DOUBLE, grid_data);
    if (status != MPI_SUCCESS) {
        LOG_ERROR_MPI("MPI_Type_vector", status);
        goto fail_exit;
    }

    status = MPI_Type_commit(grid_data);
    if (status != MPI_SUCCESS) {
        LOG_ERROR_MPI("MPI_Type_commit", status);
        goto fail_exit;
    }

    return 0;

fail_exit:
    return -1;
}

int heatsim_send_grids(heatsim_t* heatsim, cart2d_t* cart) {
    /*
     * TODO: Envoyer toutes les `grid` aux autres rangs. Cette fonction
     *       est appelé pour le rang 0. Par exemple, si le rang 3 est à la
     *       coordonnée cartésienne (0, 2), alors on y envoit le `grid`
     *       à la position (0, 2) dans `cart`.
     *
     *       Il est recommandé d'envoyer les paramètres `width`, `height`
     *       et `padding` avant les données. De cette manière, le receveur
     *       peut allouer la structure avec `grid_create` directement.
     *
     *       Utilisez `cart2d_get_grid` pour obtenir la `grid` à une coordonnée.
     */

    MPI_Datatype grid_struct;
    if (setup_mpi_type_grid_struct(&grid_struct) < 0) {
        LOG_ERROR("failed to setup MPI grid struct");
        goto fail_exit;
    }

    MPI_Datatype* grid_data = malloc((heatsim->rank_count - 1) * sizeof(*grid_data));
    if (grid_data == NULL) {
        LOG_ERROR_ERRNO("malloc");
        goto fail_exit;
    }

    MPI_Request* requests = calloc(2 * (heatsim->rank_count - 1), sizeof(*requests));
    if (requests == NULL) {
        LOG_ERROR_ERRNO("calloc");
        goto free_grid_data;
    }

    MPI_Status* statuses = calloc(2 * (heatsim->rank_count - 1), sizeof(*statuses));
    if (statuses == NULL) {
        LOG_ERROR_ERRNO("calloc");
        goto fail_free_requests;
    }

    for (int rank_target = 1; rank_target < heatsim->rank_count; rank_target++) {
        int coordinates[2];
        int status = MPI_Cart_coords(heatsim->communicator, rank_target, 2, coordinates);
        if (status != MPI_SUCCESS) {
            LOG_ERROR_MPI("MPI_Cart_coords", status);
            goto fail_free_statuses;
        }

        grid_t* grid = cart2d_get_grid(cart, coordinates[0], coordinates[1]);
        if (grid == NULL) {
            LOG_ERROR("failed to get grid at (%d, %d)", coordinates[0], coordinates[1]);
            goto fail_free_statuses;
        }

        status =
            MPI_Isend(grid, 1, grid_struct, rank_target, 0, heatsim->communicator, &requests[2 * (rank_target - 1)]);
        if (status != MPI_SUCCESS) {
            LOG_ERROR_MPI("MPI_Isend", status);
            goto fail_free_statuses;
        }

        if (setup_mpi_type_grid_data(&grid_data[rank_target - 1], grid) < 0) {
            LOG_ERROR("failed to setup MPI grid data");
            goto fail_exit;
        }

        status = MPI_Isend(grid->data, 1, grid_data[rank_target - 1], rank_target, 1, heatsim->communicator,
                           &requests[2 * (rank_target - 1) + 1]);
        if (status != MPI_SUCCESS) {
            LOG_ERROR_MPI("MPI_Isend", status);
            goto fail_free_statuses;
        }
    }

    int status = MPI_Waitall(2 * (heatsim->rank_count - 1), requests, statuses);
    if (status != MPI_SUCCESS) {
        LOG_ERROR_MPI("MPI_Waitall", status);
        goto fail_free_statuses;
    }

    MPI_Type_free(&grid_struct);
    for (int rank_target = 1; rank_target < heatsim->rank_count; rank_target++) {
        status = MPI_Type_free(&grid_data[rank_target - 1]);
    }

    free(grid_data);
    free(statuses);
    free(requests);

    return 0;

fail_free_statuses:
    free(statuses);
fail_free_requests:
    free(requests);
free_grid_data:
    free(grid_data);
fail_exit:
    return -1;
}

grid_t* heatsim_receive_grid(heatsim_t* heatsim) {
    /*
     * TODO: Recevoir un `grid ` du rang 0. Il est important de noté que
     *       toutes les `grid` ne sont pas nécessairement de la même
     *       dimension (habituellement ±1 en largeur et hauteur). Utilisez
     *       la fonction `grid_create` pour allouer un `grid`.
     *
     *       Utilisez `grid_create` pour allouer le `grid` à retourner.
     */

    MPI_Datatype grid_struct;
    if (setup_mpi_type_grid_struct(&grid_struct) < 0) {
        LOG_ERROR("failed to setup MPI grid struct");
        goto fail_exit;
    }

    MPI_Request request;
    MPI_Status status;
    grid_t grid_info;

    int status_code = MPI_Irecv(&grid_info, 1, grid_struct, 0, 0, heatsim->communicator, &request);
    if (status_code != MPI_SUCCESS) {
        LOG_ERROR_MPI("MPI_Irecv", status_code);
        goto fail_exit;
    }

    status_code = MPI_Wait(&request, &status);
    if (status_code != MPI_SUCCESS) {
        LOG_ERROR_MPI("MPI_Wait", status_code);
        goto fail_exit;
    }

    grid_t* grid = grid_create(grid_info.width, grid_info.height, grid_info.padding);
    if (grid == NULL) {
        LOG_ERROR("failed to create grid");
        goto fail_exit;
    }

    MPI_Datatype grid_data;
    if (setup_mpi_type_grid_data(&grid_data, grid) < 0) {
        LOG_ERROR("failed to setup MPI grid data");
        goto fail_destroy_grid;
    }

    status_code = MPI_Irecv(grid->data, 1, grid_data, 0, 1, heatsim->communicator, &request);
    if (status_code != MPI_SUCCESS) {
        LOG_ERROR_MPI("MPI_Irecv", status_code);
        goto fail_destroy_grid;
    }

    status_code = MPI_Wait(&request, &status);
    if (status_code != MPI_SUCCESS) {
        LOG_ERROR_MPI("MPI_Wait", status_code);
        goto fail_destroy_grid;
    }

    MPI_Type_free(&grid_struct);
    MPI_Type_free(&grid_data);

    return grid;

fail_destroy_grid:
    grid_destroy(grid);
fail_exit:
    return NULL;
}

static int setup_mpi_type_column(MPI_Datatype* column_vector, grid_t* grid) {
    int status = MPI_Type_vector(grid->height, 1, grid->width_padded, MPI_DOUBLE, column_vector);
    if (status != MPI_SUCCESS) {
        LOG_ERROR_MPI("MPI_Type_vector", status);
        goto fail_exit;
    }

    status = MPI_Type_commit(column_vector);
    if (status != MPI_SUCCESS) {
        LOG_ERROR_MPI("MPI_Type_commit", status);
        goto fail_exit;
    }

    return 0;

fail_exit:
    return -1;
}

int heatsim_exchange_borders(heatsim_t* heatsim, grid_t* grid) {
    assert(grid->padding == 1);

    /*
     * TODO: Échange les bordures de `grid`, excluant le rembourrage, dans le
     *       rembourrage du voisin de ce rang. Par exemple, soit la `grid`
     *       4x4 suivante,
     *
     *                            +-------------+
     *                            | x x x x x x |
     *                            | x A B C D x |
     *                            | x E F G H x |
     *                            | x I J K L x |
     *                            | x M N O P x |
     *                            | x x x x x x |
     *                            +-------------+
     *
     *       où `x` est le rembourrage (padding = 1). Ce rang devrait envoyer
     *
     *        - la bordure [A B C D] au rang nord,
     *        - la bordure [M N O P] au rang sud,
     *        - la bordure [A E I M] au rang ouest et
     *        - la bordure [D H L P] au rang est.
     *
     *       Ce rang devrait aussi recevoir dans son rembourrage
     *
     *        - la bordure [A B C D] du rang sud,
     *        - la bordure [M N O P] du rang nord,
     *        - la bordure [A E I M] du rang est et
     *        - la bordure [D H L P] du rang ouest.
     *
     *       Après l'échange, le `grid` devrait avoir ces données dans son
     *       rembourrage provenant des voisins:
     *
     *                            +-------------+
     *                            | x m n o p x |
     *                            | d A B C D a |
     *                            | h E F G H e |
     *                            | l I J K L i |
     *                            | p M N O P m |
     *                            | x a b c d x |
     *                            +-------------+
     *
     *       Utilisez `grid_get_cell` pour obtenir un pointeur vers une cellule.
     */

    double* send_top    = grid_get_cell(grid, 0, 0);
    double* send_bottom = grid_get_cell(grid, 0, grid->height - 1);
    double* send_left   = send_top;
    double* send_right  = grid_get_cell(grid, grid->width - 1, 0);

    double* recv_top    = grid_get_cell(grid, 0, -1);
    double* recv_bottom = grid_get_cell(grid, 0, grid->height);
    double* recv_left   = grid_get_cell(grid, -1, 0);
    double* recv_right  = grid_get_cell(grid, grid->width, 0);

    MPI_Datatype column_vector;
    if (setup_mpi_type_column(&column_vector, grid) < 0) {
        LOG_ERROR("failed to setup column vector type");
        goto fail_exit;
    }

    MPI_Request requests[8];
    MPI_Status statuses[8];
    unsigned int width  = grid->width;
    unsigned int height = grid->height;
    int north           = heatsim->rank_north_peer;
    int south           = heatsim->rank_south_peer;
    int west            = heatsim->rank_west_peer;
    int east            = heatsim->rank_east_peer;
    MPI_Comm comm       = heatsim->communicator;
    int status;

    /* send requests */

    status = MPI_Isend(send_top, width, MPI_DOUBLE, north, 0, comm, &requests[0]);
    if (status != MPI_SUCCESS) {
        LOG_ERROR_MPI("MPI_Isend", status);
        goto fail_exit;
    }

    status = MPI_Isend(send_bottom, width, MPI_DOUBLE, south, 1, comm, &requests[1]);
    if (status != MPI_SUCCESS) {
        LOG_ERROR_MPI("MPI_Isend", status);
        goto fail_exit;
    }

    status = MPI_Isend(send_left, 1, column_vector, west, 2, comm, &requests[2]);
    if (status != MPI_SUCCESS) {
        LOG_ERROR_MPI("MPI_Isend", status);
        goto fail_exit;
    }

    status = MPI_Isend(send_right, 1, column_vector, east, 3, comm, &requests[3]);
    if (status != MPI_SUCCESS) {
        LOG_ERROR_MPI("MPI_Isend", status);
        goto fail_exit;
    }

    /* receive requests */

    status = MPI_Irecv(recv_bottom, width, MPI_DOUBLE, south, 0, comm, &requests[4]);
    if (status != MPI_SUCCESS) {
        LOG_ERROR_MPI("MPI_Irecv", status);
        goto fail_exit;
    }

    status = MPI_Irecv(recv_top, width, MPI_DOUBLE, north, 1, comm, &requests[5]);
    if (status != MPI_SUCCESS) {
        LOG_ERROR_MPI("MPI_Irecv", status);
        goto fail_exit;
    }

    status = MPI_Irecv(recv_right, 1, column_vector, east, 2, comm, &requests[6]);
    if (status != MPI_SUCCESS) {
        LOG_ERROR_MPI("MPI_Irecv", status);
        goto fail_exit;
    }

    status = MPI_Irecv(recv_left, 1, column_vector, west, 3, comm, &requests[7]);
    if (status != MPI_SUCCESS) {
        LOG_ERROR_MPI("MPI_Irecv", status);
        goto fail_exit;
    }

    /* wait for all request */

    status = MPI_Waitall(8, requests, statuses);
    if (status != MPI_SUCCESS) {
        LOG_ERROR_MPI("MPI_Waitall", status);
        goto fail_exit;
    }

    MPI_Type_free(&column_vector);

    return 0;

fail_exit:
    return -1;
}

int heatsim_send_result(heatsim_t* heatsim, grid_t* grid) {
    assert(grid->padding == 0);

    /*
     * TODO: Envoyer le `grid` résultant au rang 0. Le `grid` n'a
     *       aucun rembourage (padding = 0);
     */

    MPI_Request request;
    MPI_Status status;

    unsigned int count = grid->width * grid->height;
    int status_code    = MPI_Isend(grid->data, count, MPI_DOUBLE, 0, 0, heatsim->communicator, &request);
    if (status_code != MPI_SUCCESS) {
        LOG_ERROR_MPI("MPI_Isend", status_code);
        goto fail_exit;
    }

    status_code = MPI_Wait(&request, &status);
    if (status_code != MPI_SUCCESS) {
        LOG_ERROR_MPI("MPI_Wait", status_code);
        goto fail_exit;
    }

    return 0;

fail_exit:
    return -1;
}

int heatsim_receive_results(heatsim_t* heatsim, cart2d_t* cart) {
    /*
     * TODO: Recevoir toutes les `grid` des autres rangs. Aucune `grid`
     *       n'a de rembourage (padding = 0).
     *
     *       Utilisez `cart2d_get_grid` pour obtenir la `grid` à une coordonnée.
     */

    for (int rank_target = 1; rank_target < heatsim->rank_count; rank_target++) {
        int coordinates[2];
        int status_code = MPI_Cart_coords(heatsim->communicator, rank_target, 2, coordinates);
        if (status_code != MPI_SUCCESS) {
            LOG_ERROR_MPI("MPI_Cart_coords", status_code);
            goto fail_exit;
        }

        grid_t* grid = cart2d_get_grid(cart, coordinates[0], coordinates[1]);
        if (grid == NULL) {
            LOG_ERROR("failed to get grid at (%d, %d)", coordinates[0], coordinates[1]);
            goto fail_exit;
        }

        MPI_Request request;
        MPI_Status status;

        unsigned int count = grid->width * grid->height;
        status_code        = MPI_Irecv(grid->data, count, MPI_DOUBLE, rank_target, 0, heatsim->communicator, &request);
        if (status_code != MPI_SUCCESS) {
            LOG_ERROR_MPI("MPI_Isend", status_code);
            goto fail_exit;
        }

        status_code = MPI_Wait(&request, &status);
        if (status_code != MPI_SUCCESS) {
            LOG_ERROR_MPI("MPI_Wait", status_code);
            goto fail_exit;
        }
    }

    return 0;

fail_exit:
    return -1;
}
