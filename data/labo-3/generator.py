#!/usr/bin/env python3

import argparse
import random
import tempfile
import subprocess
import string
import flask
import flask_limiter
import flask_cors
import os.path
import functools

######################
# handout generation #
######################

send_grids = "L'envoit/réception des grilles initiales doit {}.\n"
send_grids+= "Vous devez envoyé les paramètres `width`, `height` et `padding` {}.\n"
send_grids+= "Les données (`data`) de la grille doivent être envoyé en une seule requête {}.\n\n"
send_grids_mode = [
    "être effectué avec `MPI_Send` et `MPI_Recv`",
    "être effectué avec `MPI_Isend` et `MPI_Irecv`",
    "être effectué avec `MPI_Isend` et `MPI_Irecv`",
]
send_grids_params = [
    "en trois requêtes séparées de type `MPI_UNSIGNED`",
    "en une seule requête de type `MPI_UNSIGNED`",
    "en une seule requête de type définit avec `MPI_Type_struct`",
]
send_grids_data = [
    "de type définit avec `MPI_Type_contiguous`",
    "de type définit avec `MPI_Type_struct`",
    "de type `MPI_DOUBLE`",
]

exchange = "L'échange des bordures doit {}.\n"
exchange+= "Les bordures nord et sud doivent être {}.\n"
exchange+= "Les bordures est et ouest doivent être {}.\n\n"
exchange_mode = [
    "être effectué avec `MPI_Isend` et `MPI_Irecv`",
    "être effectué avec `MPI_Isend` et `MPI_Irecv`",
]
exchange_ns = [
    "de type définit avec `MPI_Type_contiguous`",
    "de type `MPI_DOUBLE`",
]
exchange_eo = [
    "de type définit avec `MPI_Type_vector`",
]

receive_grid = "L'envoie et la réception de la grille finale doit {}.\n"
receive_grid+= "Les données (`data`) de la grille doit être {}.\n\n"
receive_grid_mode = [
    "être effectué avec `MPI_Send` et `MPI_Recv`",
    "être effectué avec `MPI_Isend` et `MPI_Irecv`",
    "être effectué avec `MPI_Isend` et `MPI_Irecv`",
]
receive_grid_type = [
    "de type définit avec `MPI_Type_contiguous`",
    "de type définit avec `MPI_Type_struct`",
    "de type `MPI_DOUBLE`",
]

def check_return_code(completed):
    if completed.returncode != 0:
        print('FAILED EXECUTING `%s`'.format(completed.args))
        print('PROCESS RETURNED CODE %d'.format(completed.returncode))
        exit(1)

def generate_handout(other_directory, target_filename, delete=True, students=[]):
    tmp_dir = tempfile.mkdtemp()
    build_dir = tmp_dir + "/build"

    check_return_code(subprocess.run(['cp', '-arv', "reference/.", tmp_dir]))
    if other_directory is not None:
        check_return_code(subprocess.run(['cp', '-arv', other_directory, tmp_dir]))
    check_return_code(subprocess.run(['mkdir', '-p', build_dir]))

    if len(students) > 0:
        with open(tmp_dir + "/README-students.asc", 'w') as f:
            for student in students:
                f.write('[.text-center]\n')
                f.write('_Pour {}_\n\n'.format(student))

    with open(tmp_dir + "/README-specs.asc", 'w') as f:
        mode = random.choice(send_grids_mode)
        params = random.choice(send_grids_params)
        data = random.choice(send_grids_data)
        f.write(send_grids.format(mode, params, data))

        mode = random.choice(exchange_mode)
        ns = random.choice(exchange_ns)
        eo = random.choice(exchange_eo)
        f.write(exchange.format(mode, ns, eo))

        mode = random.choice(receive_grid_mode)
        type = random.choice(receive_grid_type)
        f.write(receive_grid.format(mode, type))

    check_return_code(subprocess.run(['cmake', '..'], cwd=build_dir))
    check_return_code(subprocess.run(['make', 'tests'], cwd=build_dir))
    check_return_code(subprocess.run(['make', 'handout'], cwd=build_dir))

    check_return_code(subprocess.run(['cp', build_dir + '/handout.zip', target_filename]))

    if delete:
        check_return_code(subprocess.run(['rm', '-rf', tmp_dir]))
    else:
        print('temporary files location: {}'.format(tmp_dir))

##################
# email checking #
##################

def check_emails():
    emails = None
    if os.path.exists('emails.txt'):
        with open('emails.txt', 'rt') as f:
            emails = [email.strip() for email in f.readlines()]

    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            if emails is None:
                return func(*args, **kwargs)

            email1 = flask.request.args.get('email1', default='', type=str)
            if email1 not in emails:
                return ('unknown email `{}`'.format(email1), 404)

            email2 = flask.request.args.get('email2', default='', type=str)
            if email2 not in emails and email2 != '':
                return ('unknown email `{}`'.format(email2), 404)

            return func(*args, **kwargs)
        return wrapper
    return decorator

##############
# web server #
##############

app = flask.Flask(__name__)
limiter = flask_limiter.Limiter(app, key_func=flask_limiter.util.get_remote_address)
cors = flask_cors.CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

@app.route('/', methods=['GET'])
@flask_cors.cross_origin()
@check_emails()
@limiter.limit("1 per minute")
def generate():
    email1 = flask.request.args.get('email1', default='', type=str)
    email2 = flask.request.args.get('email2', default='', type=str)
    print('received emails `{}` and `{}`'.format(email1, email2))

    if email1 == '':
        return 'missing email'
    elif email2 == '':
        students = [email1]
        email2 = email1[::-1]
    else:
        students = [email1, email2]

    length = min(len(email1), len(email2))
    email1 = [ord(email1[i]) for i in range(length)]
    email2 = [ord(email2[i]) for i in range(length)]
    xored = [email1[i] ^ email2[i] for i in range(length)]

    seed = sum(xored)
    random.seed(seed)

    print('seed is `{}`'.format(seed))
    generate_handout(None, 'handout.zip', students=students)

    return flask.send_file('handout.zip', as_attachment=True)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate handouts for students')
    parser.add_argument('--no-delete', action='store_true', help='don\' delete temporary files')
    parser.add_argument('--solution', action='store_true', help='generate solution handout')
    args = parser.parse_args()

    if args.solution:
        generate_handout('solution/.', 'handout-solution.zip', delete=(not args.no_delete))
    else:
        if os.path.exists('server.crt') and os.path.exists('server.key'):
            print('running server in HTTPS mode')
            app.run(threaded=False, processes=1, host='0.0.0.0', ssl_context=('server.crt', 'server.key'))
        else:
            print('running server in HTTP mode')
            app.run(threaded=False, processes=1, host='0.0.0.0')
