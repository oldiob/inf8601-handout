(define-module (tools common)
  #:export (success-value)
  #:export-syntax (setenv*))

(define-syntax-rule (setenv* (name value) ...)
  (begin
    (setenv name value) ...))

(define (success-value status)
  (or (status:exit-val status)
      (status:term-sig status)))
